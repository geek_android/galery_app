package com.martin.gallery.network;

import android.content.Context;
import android.util.Log;

import com.martin.gallery.MainActivity;
import com.martin.gallery.network.ImgurMap.Image;
import com.martin.gallery.network.ImgurMap.ImgurTagGallery;
import com.martin.gallery.network.ImgurMap.Item;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImgurGalleryLoader {
    private static final String API_KEY = "295ac460854bb1f";
    private static final String IMGUR_BASE_URL = "https://api.imgur.com/";


    public static List<Image> getImageList(final Context context, String tagName) {
        ImgurTagGallery imgurTagGallery = null;
        List<String> imageUrlList = null;
        Response<ImgurTagGallery> response;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(IMGUR_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ImgurApi imgurApi = retrofit.create(ImgurApi.class);

        Call<ImgurTagGallery> call = imgurApi.getGalleryByTag(tagName, API_KEY);

        try {
            response = call.execute();
            if (response.isSuccessful()) {
                imgurTagGallery = response.body();
            } else return null;
        } catch (IOException e) {
            Log.e(MainActivity.APP_TAG, e.getMessage());
        }

        List<Item> itemList = imgurTagGallery.getData().getItems();
        List<Image> imageList = new ArrayList<>();

        if (itemList != null) {
            for (Item item : itemList) {
                if (item.getImages() != null) imageList.addAll(item.getImages());
            }
        }

        //Delete everything animated from imageList
        Iterator<Image> iterator = imageList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getAnimated()) iterator.remove();
        }

        return imageList.size() > 0 ? imageList : null;
    }


    public static List<String> getPhotoUrlList(final Context context, String tagName) {
        ImgurTagGallery imgurTagGallery = null;
        List<String> imageUrlList = null;
        Response<ImgurTagGallery> response;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(IMGUR_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ImgurApi imgurApi = retrofit.create(ImgurApi.class);

        Call<ImgurTagGallery> call = imgurApi.getGalleryByTag(tagName, API_KEY);

        try {
            response = call.execute();
            if (response.isSuccessful()) {
                imgurTagGallery = response.body();
            } else return null;
        } catch (IOException e) {
            Log.e(MainActivity.APP_TAG, e.getMessage());
        }

        List<Item> itemList = imgurTagGallery.getData().getItems();
        List<Image> imageList = new ArrayList<>();

        if (itemList != null) {
            for (Item item : itemList) {
                if (item.getImages() != null) imageList.addAll(item.getImages());
            }

            imageUrlList = new ArrayList<>();
            for (Image image : imageList) {
                imageUrlList.add(image.getLink());
            }
        }

        return imageUrlList;
    }

}
