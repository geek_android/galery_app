package com.martin.gallery.network;

import com.martin.gallery.network.ImgurMap.ImgurTagGallery;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ImgurApi {

    @GET("3/gallery/t/{t_name}/viral/year/0")
    Call<ImgurTagGallery> getGalleryByTag(@Path("t_name") String tagName, @Query("client_id") String clienIdKey);
}
