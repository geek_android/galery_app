package com.martin.gallery;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martin.gallery.RecyclerViewAdapters.FavImagesRVAdapter;
import com.martin.gallery.database.AppDb;
import com.martin.gallery.network.ImgurMap.Image;

import java.util.List;


public class FavoriteImageFragment extends Fragment {
    FavImagesRVAdapter favImagesRVAdapter;
    AppDb appDb;
    private Handler handler = new Handler();

    public FavoriteImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appDb = new AppDb(getContext());
        appDb.openDb();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_favorite_image, container, false);
        loadFavImageFromDb(rootView);
        return rootView;
    }

    @Override
    public void onPause() {
        if (favImagesRVAdapter != null) {
            appDb.insertFavImages(favImagesRVAdapter.getFavoriteImagesList());
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        appDb.close();
    }

    private void loadFavImageFromDb(final View rootView) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Image> favImageList = appDb.getFavImageListFromDb();
                if (favImageList == null || favImageList.isEmpty()) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.showToast(getContext(), "You don't have favorite images");
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            renderFavImageFragment(rootView, favImageList);
                        }
                    });
                }
            }
        }).start();
    }

    public void renderFavImageFragment(View rootView, List<Image> favImageList) {
        RecyclerView favImgRecyclerView = rootView.findViewById(R.id.fav_image_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        favImgRecyclerView.setLayoutManager(linearLayoutManager);
        favImagesRVAdapter = new FavImagesRVAdapter(favImageList);
        favImgRecyclerView.setAdapter(favImagesRVAdapter);
        favImagesRVAdapter.notifyDataSetChanged();
    }
}
