package com.martin.gallery;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martin.gallery.RecyclerViewAdapters.GalleryRVAdapter;
import com.martin.gallery.database.AppDb;
import com.martin.gallery.network.ImgurGalleryLoader;
import com.martin.gallery.network.ImgurMap.Image;

import java.util.List;

public class ImguGalleryFragment extends Fragment {

    private static final String TAG_KEY = "tag_key";
    private final Handler handler = new Handler();
    private String tagSearchTitle;
    GalleryRVAdapter galleryRVAdapter;
    private AppDb appDb;

    static ImguGalleryFragment newInstance(String tagSearchTitle) {
        ImguGalleryFragment imguGalleryFragment = new ImguGalleryFragment();
        Bundle args = new Bundle();
        args.putString(TAG_KEY, tagSearchTitle);
        imguGalleryFragment.setArguments(args);
        return imguGalleryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appDb = new AppDb(getContext());
        appDb.openDb();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_imgur_gallery, container, false);


        //TODO исправить, чтобы брал из SharedPreferences или не рисовал ничего
        tagSearchTitle = getArguments() != null ? getArguments().getString(TAG_KEY) : "";
        if (!tagSearchTitle.isEmpty()) {
            getImagesFromServer(rootView, tagSearchTitle);
        } else MainActivity.showToast(getContext(), "Enter some tag for search");
        return rootView;
    }

    @Override
    public void onPause() {
        if (galleryRVAdapter != null) {
            appDb.insertFavImages(galleryRVAdapter.getFavoriteImagesList());
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        appDb.close();
    }

    public void getImagesFromServer(final View rootView, final String tagNameForSearch) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //TODO Сделать в 2 разных потоках запросы к БД и к Апи и подождать

                final List<Image> imageList = ImgurGalleryLoader.getImageList(getContext(), tagNameForSearch);
                final List<Image> favImageSet = appDb.getFavImageListFromDb();

                if (imageList == null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.showToast(getContext(), "Internet problem or bad query");
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            initRecyclerView(rootView, imageList, favImageSet);
                            saveToSharedPreferences(tagNameForSearch);
                        }
                    });
                }
            }
        }).start();

    }

    private void saveToSharedPreferences(String tagNameForSearch) {
        SharedPreferences.Editor SpEditor = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
        SpEditor.putString(MainActivity.KEY_FOR_TAG_SEARCH, tagNameForSearch);
        SpEditor.apply();
    }

    public void initRecyclerView(View rootView, List<Image> imageList, List<Image> favImageSet) {
        RecyclerView mRecyclerView = rootView.findViewById(R.id.gallery_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        galleryRVAdapter = new GalleryRVAdapter(imageList, favImageSet);
        mRecyclerView.setAdapter(galleryRVAdapter);
        galleryRVAdapter.notifyDataSetChanged();
    }
}
