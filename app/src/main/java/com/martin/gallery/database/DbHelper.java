package com.martin.gallery.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    static final String TABLE_FAV_IMAGE = "fav_images";
    static final String IMAGE_JSON_COLUMN = "image_json";
    private static final String DB_NAME = "favorite_gallery.db";
    private static final int DB_VERSION = 1;
    private static final String ID_COLUMN = "id";
    private static final String FAV_IMAGE_CREATE_TABLE = "CREATE TABLE " + TABLE_FAV_IMAGE + " (\n" +
            "    " + ID_COLUMN + "         INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL,\n" +
            "    " + IMAGE_JSON_COLUMN + " TEXT    NOT NULL);";

    private static final String DROP_TABLE = "DROP TABLE " + TABLE_FAV_IMAGE;

    DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FAV_IMAGE_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < DB_VERSION) db.execSQL(DROP_TABLE);
        onCreate(db);
    }
}
