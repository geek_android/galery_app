package com.martin.gallery.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.martin.gallery.network.ImgurMap.Image;

import java.util.ArrayList;
import java.util.List;

public class AppDb {
    private Gson gson;
    private DbHelper dbHelper;
    private SQLiteDatabase db;

    public AppDb(Context context) {
        dbHelper = new DbHelper(context);
        gson = new Gson();
    }

    public void openDb() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void insertFavImages(List<Image> favImagesSet) {
        //TODO Удалить надо и добавить новые, так как если не удалю то повторы будут
        db.delete(DbHelper.TABLE_FAV_IMAGE, null, null);

        //TODO возмодно за один раз передать, сразу пачкой
        for (Image image : favImagesSet) {
            ContentValues values = new ContentValues();
            String imageJson = gson.toJson(image);
            values.put(DbHelper.IMAGE_JSON_COLUMN, imageJson);
            db.insert(DbHelper.TABLE_FAV_IMAGE, null, values);
        }
    }

    public List<Image> getFavImageListFromDb() {
        List<Image> imageListFromDb = new ArrayList<>();
        String query = "SELECT " + DbHelper.IMAGE_JSON_COLUMN + " FROM " + DbHelper.TABLE_FAV_IMAGE;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                String imageJson = cursor.getString(0);
                imageListFromDb.add(gson.fromJson(imageJson, Image.class));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return imageListFromDb;
    }


}
