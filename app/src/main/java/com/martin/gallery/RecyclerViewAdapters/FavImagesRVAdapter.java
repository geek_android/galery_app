package com.martin.gallery.RecyclerViewAdapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martin.gallery.R;
import com.martin.gallery.network.ImgurMap.Image;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FavImagesRVAdapter extends RecyclerView.Adapter<PhotoViewHolder> {
    private List<Image> favoriteImagesList;
    private List<Image> removeFromFavImageList;
    private int widthPixels;

    public FavImagesRVAdapter(List<Image> favoriteImagesList) {
        this.favoriteImagesList = favoriteImagesList;
        removeFromFavImageList = new ArrayList<>();
        widthPixels = Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PhotoViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_grid_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        Picasso.get()
                .load(favoriteImagesList.get(position).getLink())
                .resize(widthPixels, 0)
                .placeholder(R.drawable.placeholder_img)
                .into(holder.photoImageView);
        holder.changeStarToYellow();
        setFavImageListenerToStar(holder, position);
    }

    @Override
    public int getItemCount() {
        return favoriteImagesList.size();
    }

    private void setFavImageListenerToStar(final PhotoViewHolder holder, final int position) {
        holder.setStarListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Image currentImageFromList = favoriteImagesList.get(position);
                if (favoriteImagesList.contains(currentImageFromList)) {
                    removeFromFavImageList.add(currentImageFromList);
                    holder.changeStarTransparent();
                } else {
                    removeFromFavImageList.remove(currentImageFromList);
                    holder.changeStarToYellow();
                }
            }
        });
    }

    public List<Image> getFavoriteImagesList() {
        favoriteImagesList.removeAll(removeFromFavImageList);
        return favoriteImagesList;
    }
}
