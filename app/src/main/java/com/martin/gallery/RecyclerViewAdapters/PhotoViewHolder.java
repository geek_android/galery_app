package com.martin.gallery.RecyclerViewAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.martin.gallery.R;

class PhotoViewHolder extends RecyclerView.ViewHolder {
    ImageView photoImageView;

    ImageView starIcon;

    PhotoViewHolder(View itemView) {
        super(itemView);
        photoImageView = itemView.findViewById(R.id.photo_image_view);
        starIcon = itemView.findViewById(R.id.ic_star);
    }

    public void setListener(View.OnClickListener listener) {
        itemView.setOnClickListener(listener);
    }

    public void setStarListener(View.OnClickListener listener) {
        starIcon.setOnClickListener(listener);
    }

    public void changeStarToYellow() {
        starIcon.setImageResource(R.drawable.ic_star_yellow);
    }

    public void changeStarTransparent() {
        starIcon.setImageResource(R.drawable.ic_star_white);
    }
}