package com.martin.gallery.RecyclerViewAdapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martin.gallery.R;
import com.martin.gallery.network.ImgurMap.Image;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class GalleryRVAdapter extends RecyclerView.Adapter<PhotoViewHolder> {
    private List<Image> imagesList;
    private List<Image> favoriteImagesList;
    private int widthPixels;

    public GalleryRVAdapter(List<Image> imagesList, List<Image> favoriteImagesList) {
        this.imagesList = imagesList;
        widthPixels = Resources.getSystem().getDisplayMetrics().widthPixels;

        if (favoriteImagesList == null) {
            this.favoriteImagesList = new ArrayList<>();
        } else this.favoriteImagesList = favoriteImagesList;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PhotoViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_grid_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        Picasso.get()
                .load(imagesList.get(position).getLink())
                .resize(widthPixels, 0)
                .placeholder(R.drawable.placeholder_img)
                .into(holder.photoImageView);
        checkInGalleryIfImageIsFavorite(holder, position);
        setGalleryListenerToStar(holder, position);
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    private void checkInGalleryIfImageIsFavorite(PhotoViewHolder holder, int position) {
        Image currentImageFromList = imagesList.get(position);
        if (favoriteImagesList.contains(currentImageFromList)) {
            holder.changeStarToYellow();
        } else {
            holder.changeStarTransparent();
        }
    }

    private void setGalleryListenerToStar(final PhotoViewHolder holder, final int position) {
        holder.setStarListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Image currentImageFromList = imagesList.get(position);
                if (favoriteImagesList.contains(currentImageFromList)) {
                    favoriteImagesList.remove(currentImageFromList);
                    holder.changeStarTransparent();
                } else {
                    favoriteImagesList.add(currentImageFromList);
                    holder.changeStarToYellow();
                }
            }
        });
    }

    public List<Image> getFavoriteImagesList() {
        return favoriteImagesList;
    }
}
