package com.martin.gallery;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Toast;

import com.lapism.searchview.Search;
import com.lapism.searchview.widget.SearchView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    ImguGalleryFragment imguGalleryFragment;
    FavoriteImageFragment favoriteImageFragment;
    DownloadImagesFragment downloadImagesFragment;
    FragmentManager fragmentManager;

    public static final String APP_TAG = "gal_tag";
    public static final String KEY_FOR_TAG_SEARCH = "KEY_FOR_TAG_SEARCH";
    List<String> urlList = null;

    @BindView(R.id.drawer_layout2)
    DrawerLayout drawer;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    public static void showToast(Context context, String textForToast) {
        Toast.makeText(context, textForToast, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initNavDrawer();

        String searchTag = getSearchTagFromSharedPreferences();
        searchView.setText(searchTag);

//        initMainGalleryFragment(searchTag);
        initBottomNavView(searchTag);
        initSearchView();
    }

    private void initBottomNavView(String searchTag) {
        imguGalleryFragment = ImguGalleryFragment.newInstance(searchTag);
        favoriteImageFragment = new FavoriteImageFragment();
        downloadImagesFragment = new DownloadImagesFragment();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_for_fragment, imguGalleryFragment);
        fragmentTransaction.commit();

        BottomNavigationView bnv = findViewById(R.id.bnv);
        bnv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_search:
                        FragmentTransaction fragmentTransactionS = fragmentManager.beginTransaction();
                        fragmentTransactionS.replace(R.id.container_for_fragment, imguGalleryFragment);
                        fragmentTransactionS.commit();
                        return true;
                    case R.id.action_favorite:
                        searchView.setText("");
                        FragmentTransaction fragmentTransactionF = fragmentManager.beginTransaction();
                        fragmentTransactionF.replace(R.id.container_for_fragment, favoriteImageFragment);
                        fragmentTransactionF.commit();
                        return true;
                    case R.id.action_downloads:
                        searchView.setText("");
                        FragmentTransaction fragmentTransactionD = fragmentManager.beginTransaction();
                        fragmentTransactionD.replace(R.id.container_for_fragment, downloadImagesFragment);
                        fragmentTransactionD.commit();
                        return true;
                }
                return false;
            }
        });
    }

    private void initSearchView() {
        searchView.setOnLogoClickListener(getOnSearchViewListener());
        searchView.setOnQueryTextListener(getOnSearchViewQueryListener());
    }

    @NonNull
    private Search.OnQueryTextListener getOnSearchViewQueryListener() {
        return new Search.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(CharSequence query) {
                imguGalleryFragment = ImguGalleryFragment.newInstance(query.toString());
                FragmentTransaction fragmentTransactionS = fragmentManager.beginTransaction();
                fragmentTransactionS.replace(R.id.container_for_fragment, imguGalleryFragment);
                fragmentTransactionS.commit();
                searchView.close();
                return true;
//
//                ImguGalleryFragment imguGalleryFragment = ImguGalleryFragment.newInstance(query.toString());
//                getSupportFragmentManager().beginTransaction().
//                        replace(R.id.container_for_fragment, imguGalleryFragment).commit();
//                return false;
            }

            @Override
            public void onQueryTextChange(CharSequence newText) {

            }
        };
    }

    @NonNull
    private Search.OnLogoClickListener getOnSearchViewListener() {
        return new Search.OnLogoClickListener() {
            @Override
            public void onLogoClick() {
                if (!drawer.isDrawerVisible(Gravity.START)) drawer.openDrawer(Gravity.START);
                else drawer.closeDrawer(Gravity.START);
            }
        };
    }

    private String getSearchTagFromSharedPreferences() {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        String searchTag = null;
        if (sharedPreferences != null) {
            searchTag = sharedPreferences.getString(KEY_FOR_TAG_SEARCH, "");
        }
        return searchTag;
    }

    private void initNavDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(getNavItemListener());
    }

    private void initMainGalleryFragment(String tagForSearch) {
        ImguGalleryFragment imguGalleryFragment = ImguGalleryFragment.newInstance(tagForSearch);
        getSupportFragmentManager().beginTransaction().
                replace(R.id.container_for_fragment, imguGalleryFragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getNavItemListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                Fragment fragment;

                switch (id) {
                    case R.id.favorite_images:
                        fragment = new FavoriteImageFragment();
                        break;
                    default:
                        fragment = new ImguGalleryFragment();
                        break;
                }

                drawer.closeDrawer(GravityCompat.START);
                getSupportFragmentManager().beginTransaction().
                        replace(R.id.container_for_fragment, fragment).commit();
                return true;
            }
        };
    }
}
